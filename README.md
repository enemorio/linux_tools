# linux_tools


## Getting started

- .bashrc
- .tmux.conf
- .vimrc
- bash shortcuts: https://www.gnu.org/software/bash/manual/bash.html
- interesting tools, links and repos
-     direnv: https://direnv.net/
-     jq: https://github.com/jqlang/jq
-     csvtool: https://github.com/maroofi/csvtool/

- SCRIPTDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
- my_dir=`dirname $0`
- MYDIR="$(dirname "$(realpath "$0")")"
  markdown reference: https://docs.gitlab.com/ee/user/markdown.html
