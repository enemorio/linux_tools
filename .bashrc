SSH_PRIVATE_KEY=$(cat ~/.ssh/<your ssh key>)

# --- prevent double ssh key agent loading if using tmux ---
if [[ -x $TERM_PROGRAM ]]; then
    eval $(ssh-agent -s)
    ssh-add <(echo "${SSH_PRIVATE_KEY}")
    tmux
fi


# --- variables ---
export BIN=~/local/bin
export INCLUDE=~/local/include
export REPOS=~/repos
export PATH=${PATH}:${BIN}
export CD_PATH=~/devel:~/devel/repos


# --- functions ---
for file in $(ls ${INCLUDE}/*.func); do
    . ${file}
done


# --- aliases ---
for file in $(ls ${INCLUDE}/*.alias); do
    . ${file}
done


# --- PS1 ---
#PS1="\$?: ${yellow}\w\n${b_green}${USER}@$(hostname)${reset}\$(git_status)$ "
PS1="\[\e]0;\u@\h: \w\a\]\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]$ "
